﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledToMmLvl
{
    class TileSet
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Source { get; set; }
    }
}
