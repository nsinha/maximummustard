﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace TiledToMmLvl
{
    class Program
    {

        static readonly Dictionary<int, TileSet> Tiles = new Dictionary<int, TileSet>();
        private static DemoLevel _level;
        private static readonly List<WorldItem> WorldItems = new List<WorldItem>(); 
        static void Main(string[] args)
        {
            FileInfo fi = new FileInfo(args[0]);
            if (!fi.Exists)
            {
                Console.WriteLine("{0} not found", args[0]);
                return;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(args[0]);
            var header = doc.FirstChild;
            XmlElement map = header.NextSibling as XmlElement;
            if (map == null)
            {
                Console.WriteLine("Map not found");
                return;
            }
            //int width = int.Parse(map.GetAttribute("width"));
            //int height = int.Parse(map.GetAttribute("height"));
            
            XmlElement node = map.FirstChild as XmlElement;
            while (node != null)
            {
                if (node.Name == "tileset")
                {
                    HandleTileset(node);
                }
                else if (node.Name == "layer")
                {
                    HandleLayer(node);
                }
                node = node.NextSibling as XmlElement;
            }
            var textures = Tiles.OrderBy(x => x.Key).Select(x => x.Value.Source).ToList();
            textures.Insert(0, "null");
            _level = new DemoLevel
            {
                Textures = textures.ToArray(),
                World = WorldItems.ToArray()
            };
            string json = JsonConvert.SerializeObject(_level, Formatting.Indented);
            StreamWriter writer = new StreamWriter(args[1]);
            writer.Write(json);
            writer.Close();
        }

        private static void HandleTileset(XmlElement node)
        {
            int gid = int.Parse(node.GetAttribute("firstgid"));
            XmlElement image = node.FirstChild as XmlElement;
            TileSet ts = new TileSet
            {
                Source = image.GetAttribute("source"),
                Width = int.Parse(image.GetAttribute("width")),
                Height = int.Parse(image.GetAttribute("height"))
            };
            Tiles[gid] = ts;
        }

        private static void HandleLayer(XmlElement node)
        {
            int width = int.Parse(node.GetAttribute("width"));
            int height = int.Parse(node.GetAttribute("height"));
            XmlElement data = node.FirstChild as XmlElement;
            int location = 0;
            foreach (XmlNode childNode in data.ChildNodes)
            {
                XmlElement tile = childNode as XmlElement;
                int gid = int.Parse(tile.GetAttribute("gid"));
                if (gid == 0)
                {
                    ++location;
                    continue;
                }
                TileSet ts = Tiles[gid];
                float physWidth = ts.Width / 32.0f;
                float physHeight = ts.Height / 32.0f;

                int x = location % width;
                int y = location / width;

                float physCenterX = x + (physWidth/2.0f);
                float physCenterY = y - (physHeight / 2.0f);
                WorldItem item = new WorldItem
                {
                    height = physHeight,
                    tex = gid,
                    width = physWidth,
                    x=physCenterX,
                    y=physCenterY
                };
                WorldItems.Add(item);
                ++location;
            }
        }
    }
}
