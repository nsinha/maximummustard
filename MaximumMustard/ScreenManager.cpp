#include "ScreenManager.h"


ScreenManager::ScreenManager() : _currentScreen(nullptr)
{
}


ScreenManager::~ScreenManager()
{
}

void ScreenManager::SwitchToScreen(BaseScreen* screen)
{
	_currentScreen = screen;
}

BaseScreen* ScreenManager::GetCurrentScreen()
{
	return _currentScreen;
}