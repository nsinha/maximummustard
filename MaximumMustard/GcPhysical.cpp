#include "GcPhysical.h"
#include <Box2D/Box2D.h>
#include "GameObject.h"
#include <iostream>

GcPhysical::GcPhysical() : GameComponent(GameComponentGroup::Physical), _onGround(0)
{
	AddType(GameComponentType::Physical);
}

void GcPhysical::InitializeShapes(float x, float y, float width, float height,
						bool isStatic, b2World* world, bool collidable)
{
	_x = x;
	_y = y;
	_width = width;
	_height = height;
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(x, y);
	bodyDef.type = isStatic? b2_staticBody : b2_dynamicBody;
	bodyDef.fixedRotation = true;
	b2Body* body = world->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(width / 2.f, height / 2.f); // Creates a box shape. Divide your desired width and height by 2.

	b2FixtureDef fixtureDef;
	if (isStatic)
	{
		fixtureDef.density = 0.f;
	}
	else
	{
		fixtureDef.density = 1.f;  // Sets the density of the body
		//fixtureDef.friction = 0.7f;
	}

	if (!collidable)
	{
		fixtureDef.filter.maskBits = 0x0000;
	}

	fixtureDef.shape = &shape; // Sets the shape
	auto fixture = body->CreateFixture(&fixtureDef); // Apply the fixture definition
	fixture->SetUserData(static_cast<void*>(this));
	_body = body;
}

void GcPhysical::CreateArrow(b2World* world, float x, float y, float dx, float dy, float velocity)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(x, y);
	bodyDef.type = b2_dynamicBody;
	bodyDef.bullet = true;
	bodyDef.fixedRotation = false;
	b2Body* body = world->CreateBody(&bodyDef);

	b2PolygonShape polygonShape;
	float width = 0.46875f / 4.f;
	float height = 0.1875f / 2.f;
	b2Vec2 vertices[4];
	vertices[0].Set(-width*3.f, 0);
	vertices[1].Set(0, -height);
	vertices[2].Set(width, 0);
	vertices[3].Set(0, height);
	polygonShape.Set(vertices, 4);

	b2FixtureDef fixtureDef;
	fixtureDef.density = 2.f;
	fixtureDef.shape = &polygonShape;
	b2Fixture* fix = body->CreateFixture(&fixtureDef);
	fix->SetUserData(static_cast<void*>(this));
	_body = body;

	float velChange = dx - body->GetLinearVelocity().x;;
	float impulse = body->GetMass() * velChange;
	body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);

}


void GcPhysical::AddBottomSensor()
{
	b2PolygonShape shape;
	float yCenter = _height/2.f;
	shape.SetAsBox(_width/2.f, 0.01f, b2Vec2(0.f, yCenter), 0.0f);

	b2FixtureDef myFixtureDef;
	myFixtureDef.shape = &shape;
	myFixtureDef.density = 1;
	myFixtureDef.isSensor = true;
	
	b2Fixture* bottomFixture = _body->CreateFixture(&myFixtureDef);
	bottomFixture->SetUserData(static_cast<void*>(this));
}

GcPhysical::~GcPhysical()
{
}

b2Vec2 GcPhysical::GetPosition() const
{
	return _body->GetPosition();
}

float GcPhysical::GetAngle() const
{
	return _body->GetAngle();
}

void GcPhysical::Update(float secondsElapsed)
{

}

b2Body* GcPhysical::GetBody()
{
	return _body;
}

bool GcPhysical::IsOnGround() const
{ 
	//std::cout << _onGround << std::endl;
	return _onGround > 0;
}

void GcPhysical::SetIsOnGround(bool onGround)
{
	if (onGround)
	{
		++_onGround;
	}
	else
	{
		--_onGround;
	}
}