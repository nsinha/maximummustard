#include "ContactListener.h"
#include "GcPhysical.h"
#include "IHitDetector.h"

void ContactListener::BeginContact(b2Contact* contact)
{
	b2Fixture* firstFixture = contact->GetFixtureA();
	void* fixtureUserData = firstFixture->GetUserData();
	GcPhysical* phys1 = nullptr;
	GcPhysical* phys2 = nullptr;
	if (fixtureUserData != nullptr)
	{
		phys1 = static_cast<GcPhysical*>(fixtureUserData);
		if (firstFixture->IsSensor())
		{
			phys1->SetIsOnGround(true);
		}
	}

	b2Fixture* secondFixture = contact->GetFixtureB();
	fixtureUserData = secondFixture->GetUserData();
	if (fixtureUserData != nullptr)
	{
		phys2 = static_cast<GcPhysical*>(fixtureUserData);
		if (secondFixture->IsSensor())
		{
			phys2->SetIsOnGround(true);
		}
	}

	if (phys1 != nullptr
		&& phys2 != nullptr
		&& _hitDetector != nullptr)
	{
		_hitDetector->HandleHit(phys1, phys2);
	}

	
}

void ContactListener::EndContact(b2Contact* contact)
{

	b2Fixture* firstFixture = contact->GetFixtureA();
	void* fixtureUserData = firstFixture->GetUserData();
	GcPhysical* phys1;
	GcPhysical* phys2;
	if (fixtureUserData != nullptr)
	{
		phys1 = static_cast<GcPhysical*>(fixtureUserData);
		if (firstFixture->IsSensor())
		{
			phys1->SetIsOnGround(false);
		}
	}

	b2Fixture* secondFixture = contact->GetFixtureB();
	fixtureUserData = secondFixture->GetUserData();
	if (fixtureUserData != nullptr)
	{
		phys2 = static_cast<GcPhysical*>(fixtureUserData);
		if (secondFixture->IsSensor())
		{
			phys2->SetIsOnGround(false);
		}
	}
}