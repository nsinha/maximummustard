#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include<map>
#include<list>

#ifndef GAME_COMPONENT_H
#include "GameComponent.h"
#endif



class GameObject
{
public:
	GameObject();
	virtual ~GameObject();
	unsigned int GetId() const;
	void AddComponent(GameComponent* gc);
	GameComponent* GetComponent(GameComponentGroup gcGroup) const;
	void Update(float secondsElapsed);
	void AddChild(std::string childName, GameObject* go);
	std::list<GameObject*> GetChildren();
	GameObject* GetChild(std::string name);
	void SetOwner(int playerId);
	int GetOwner() const;
	static unsigned int GetUniqueId();
private:
	unsigned int _id;
	std::map<GameComponentGroup, GameComponent*> _gcs;
	std::map<std::string, GameObject*> _children;
	GameObject* _owner;
	int _ownerId;
};

#endif