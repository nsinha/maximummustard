#ifndef GC_PHYSICAL_H
#define GC_PHYSICAL_H

#ifndef GAME_COMPONENT_H
#include "GameComponent.h"
#endif

#include <Box2D/Box2D.h>

class GcPhysical : public GameComponent
{
public:
	GcPhysical();
	void InitializeShapes(float x, float y, float width, float height, bool isStatic, b2World* world, bool collidable);
	~GcPhysical();
	b2Vec2 GetPosition() const;
	float GetAngle() const;
	virtual void Update(float secondsElapsed);
	b2Body* GetBody();
	void AddBottomSensor();
	bool IsOnGround() const;
	void SetIsOnGround(bool onGround);
	void CreateArrow(b2World* world, float x, float y, float dx, float dy, float velocity);
private:
	float _x, _y;
	float _width, _height;
	int _onGround;
	b2Body* _body;
};

#endif