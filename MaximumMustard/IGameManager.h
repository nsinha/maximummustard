#ifndef I_GAME_MANAGER_H
#define I_GAME_MANAGER_H

class GameObject;

class IGameManager
{
public:
	IGameManager();
	virtual void ChangePlayerTarget(int playerId, GameObject* go) = 0;
	virtual ~IGameManager();
};

#endif