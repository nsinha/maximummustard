#ifndef I_KILL_HANDLER_H
#define I_KILL_HANDLER_H

class IKillHandler
{
public:
	IKillHandler();
	virtual void HandleKill(int killer, int killee) = 0;
	virtual ~IKillHandler();
};

#endif