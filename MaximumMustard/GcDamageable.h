#ifndef GC_DAMAGEABLE_H
#define GC_DAMAGEABLE_H

#include "GameComponent.h"
class GcDamageable :
	public GameComponent
{
public:
	GcDamageable(int maxHealth);
	virtual ~GcDamageable();
	bool TakeDamage(int damage);
	int GetHealth() const;
	int GetMaxHealth() const;
	virtual void Update(float dt);
private:
	int _maxHealth;
	int _currentHealth;
};

#endif