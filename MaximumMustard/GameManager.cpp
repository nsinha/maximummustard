#include "GameManager.h"
#include <ostream>
#include <iostream>
#include "Camera.h"


GameManager::GameManager()
{
}

void GameManager::HandleKill(int killer, int killee)
{
	std::cout << "Kill " <<killer<<" "<<killee<< std::endl;
	for (PlayerInfo& pi : _players)
	{
		if (pi.PlayerId == killer)
		{
			++pi.Kills;
		}
		if (pi.PlayerId == killee)
		{
			++pi.Deaths;
		}
	}
}

void GameManager::ChangePlayerTarget(int playerId, GameObject* go)
{
	for (PlayerInfo& pi : _players)
	{
		if (pi.PlayerId == playerId)
		{
			pi.Go = go;
			pi.Camera->SetTarget(go);
			pi.Camera->Update(1.f);
			pi.Camera->SnapToTarget();
			break;
		}
	}
}

bool GameManager::CheckForVictory() const
{
	for (const PlayerInfo& pi : _players)
	{
		if (pi.Kills >= 10)
		{
			return true;
		}
	}
	return false;
}

void GameManager::AddPlayer(PlayerInfo pi)
{
	_players.push_back(pi);
}


std::vector<PlayerInfo>& GameManager::GetPlayers()
{
	return _players;
}

GameManager::~GameManager()
{
}
