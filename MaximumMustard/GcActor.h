#ifndef GC_ACTOR_H
#define GC_ACTOR_H

#ifndef GAME_COMPONENT_H
#include "GameComponent.h"
#endif

#ifndef STATE_MACHINE_H
#include "StateMachine.h"
#endif

class Level;
class GcPhysical;

class GcActor : public GameComponent
{
public:
	GcActor();
	~GcActor();
	void MoveLeft(float magnitude);
	void MoveRight(float magnitude);
	void Jump();
	void Shoot();
	void SetPullStrength(float pullStrength);
	void CancelPull();
	void SetLevel(Level* level);
	bool IsDrawn() const;
	virtual void Update(float secondsElapsed);
	void IncreasePullStrength();
private:
	void SetLeftBowVisiblity(bool visible);
	void SetRightBowVisiblity(bool visible);
	GcPhysical* GetPhysics();
	int _jumpCooldown;
	int _shootCooldown;
	StateMachine _state;
	int _walkFrame;
	float _pullStrength;
	Level* _level;
};

#endif