#include "GameScreen.h"
#include "ContactListener.h"
#include "GcDrawable.h"
#include "GcActor.h"
#include "GameObject.h"
#include "Level.h"
#include "Controller.h"
#include "GcDamageable.h"
#include <sstream>
#include "GameCompleteScreen.h"
#include "ScreenManager.h"
#include "Camera.h"

using namespace std;

GameScreen::GameScreen(sf::RenderWindow* window, vector<int> players) : BaseScreen(window)
{
	size_t numberOfPlayers = players.size();
	for (int i : players)
	{
		PlayerInfo pi;
		pi.PlayerId = i;
		pi.Kills = 0;
		pi.Deaths = 0;

		switch (numberOfPlayers)
		{
		case 1:
			pi.PlayerView.setSize(static_cast<float>(_window->getSize().x), static_cast<float>(_window->getSize().y));
			pi.PlayerView.setCenter(static_cast<float>(_window->getSize().x) / 2.f, static_cast<float>(_window->getSize().y) / 2.f);
			pi.PlayerView.setViewport(sf::FloatRect(0.f, 0.f, 1.f, 1.f));
			break;
		case 2:
			pi.PlayerView.setSize(static_cast<float>(_window->getSize().x) * 2.f, static_cast<float>(_window->getSize().y));
			pi.PlayerView.setCenter(static_cast<float>(_window->getSize().x), static_cast<float>(_window->getSize().y) / 2.f);
			switch (i)
			{
			case 0:
				pi.PlayerView.setViewport(sf::FloatRect(0.f, 0.f, 1.f, 0.5f));
				break;
			case 1:
				pi.PlayerView.setViewport(sf::FloatRect(0.f, 0.5f, 1.f, 0.5f));
				break;
			}
			break;
		case 3:
		case 4:
			pi.PlayerView.setSize(static_cast<float>(_window->getSize().x), static_cast<float>(_window->getSize().y));
			pi.PlayerView.setCenter(static_cast<float>(_window->getSize().x) / 2.f, static_cast<float>(_window->getSize().y) / 2.f);
			switch (i)
			{
			case 0:
				pi.PlayerView.setViewport(sf::FloatRect(0.f, 0.f, 0.5f, 0.5f));
				break;
			case 1:
				pi.PlayerView.setViewport(sf::FloatRect(0.5f, 0.f, 0.5f, 0.5f));
				break;
			case 2:
				pi.PlayerView.setViewport(sf::FloatRect(0.f, 0.5f, 0.5f, 0.5f));
				break;
			case 3:
				pi.PlayerView.setViewport(sf::FloatRect(0.5f, 0.5f, 0.5f, 0.5f));
				break;
			}
			break;
		}

		_gameManager.AddPlayer(pi);
	}

	_textFont.loadFromFile(".\\Assets\\Fonts\\GI-Bold.ttf");
}


GameScreen::~GameScreen()
{
	delete _level;
}

void GameScreen::Initialize()
{
	InitializeLevel();
	for (auto& pi : _gameManager.GetPlayers())
	{
		pi.Go = _level->GeneratePlayer(pi.PlayerId);
		GameComponent* gc = pi.Go->GetComponent(GameComponentGroup::Actor);
		GcActor *actor = dynamic_cast<GcActor*>(gc);
		if (actor != nullptr)
		{
			Controller* c = new Controller(actor, pi.PlayerId);
			_level->AddController(c);
		}
		sf::Vector2f size = pi.PlayerView.getSize();
		Camera* camera = new Camera(size.x, size.y);
		camera->SetTarget(pi.Go);
		camera->Update(1.f);
		camera->SnapToTarget();
		pi.Camera = camera;

	}

	_level->SetGameManager(&_gameManager);
	_level->SetKillHandler(&_gameManager);
}

bool GameScreen::Update()
{

	if (_gameManager.CheckForVictory())
	{
		vector<FinalScore> finalScore;
		for (auto& pi : _gameManager.GetPlayers())
		{
			FinalScore fs;
			stringstream ss;
			ss << "Player" << pi.PlayerId + 1;
			fs.PlayerName = ss.str();
			fs.Kills = pi.Kills;
			fs.Deaths = pi.Deaths;
			finalScore.push_back(fs);
		}

		GameCompleteScreen* gcs = new GameCompleteScreen(_window);
		gcs->SetFinalScores(finalScore);
		gcs->Initialize();
		ScreenManager::GetInstance().SwitchToScreen(gcs);
		return false;
	}
	_level->Step();
	for (auto& pi : _gameManager.GetPlayers())
	{
		pi.Camera->Update(1.f / 60.f);
		pi.PlayerView.setCenter(pi.Camera->GetCenterX() * 32.f, pi.Camera->GetCenterY() * 32.f);
	}

	return true;
}

void GameScreen::Draw()
{
	_window->clear();
	for (auto& pi : _gameManager.GetPlayers())
	{
		_window->setView(pi.PlayerView);
		auto gcs = _level->GetGameObjects(GameComponentGroup::Graphical);
		TextureManager<sf::Sprite>* textureManager = _level->GetTextureManager();
		for (auto gc : gcs)
		{
			GcDrawable* graphical = static_cast<GcDrawable*>(gc);
			if (!graphical->IsVisible())
			{
				continue;
			}
			sf::Sprite *sprite = textureManager->Get(graphical->GetTextureId());
			if (graphical->IsFlipped())
			{
				sprite->setScale(-1.f, 1.f);
			}
			else
			{
				sprite->setScale(1.f, 1.f);
			}
			sf::Vector2f vec(graphical->GetX() * 32.f, graphical->GetY() * 32.f);
			sprite->setPosition(vec);
			sprite->setRotation(graphical->GetAngle());

			_window->draw(*sprite);
		}

		GameObject* go = pi.Go;
		pi.PlayerView.setCenter(pi.PlayerView.getSize() / 2.f);
		_window->setView(pi.PlayerView);
		if (go != nullptr)
		{
			sf::Sprite *heartSprite = textureManager->Get(17);
			sf::Sprite *emptyHeartSprite = textureManager->Get(18);
			GameComponent* gc = go->GetComponent(GameComponentGroup::Damageable);
			GcDamageable* damageable = dynamic_cast<GcDamageable*>(gc);
			if (damageable != nullptr)
			{
				int maxHealth = damageable->GetMaxHealth();
				int health = damageable->GetHealth();
				int i = 0;
				for (; i < health; ++i)
				{
					sf::Vector2f vec(32.f * (i + 1) + i * 5, 32.f);
					heartSprite->setPosition(vec);
					_window->draw(*heartSprite);
				}

				for (; i < maxHealth; ++i)
				{
					sf::Vector2f vec(32.f * (i + 1) + i * 5, 32.f);
					emptyHeartSprite->setPosition(vec);
					_window->draw(*emptyHeartSprite);
				}
			}
		}

		sf::Text text;
		text.setFont(_textFont);
		text.setPosition(200.f, 16.f);
		text.setColor(sf::Color::White);
		text.setCharacterSize(32);
		stringstream ss;
		ss << "Kills: " << pi.Kills;
		std::string killString = ss.str();
		text.setString(killString);
		_window->draw(text);


	}
	_window->display();
}

void GameScreen::InitializeLevel()
{

	Level* l = new Level();
	auto world = l->GetWorld();
	l->LoadLevel(".\\Assets\\Levels\\Demo.lvl");

	ContactListener* listener = new ContactListener();
	listener->SetHitDetector(l);
	l->GetWorld()->SetContactListener(listener);

	_level = l;
}