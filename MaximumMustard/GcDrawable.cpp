#include "GcDrawable.h"

#ifndef GAME_OBJECT_H
#include "GameObject.h"
#endif

#ifndef GC_PHYSICAL_H
#include "GcPhysical.h"
#endif


GcDrawable::GcDrawable() : GameComponent(GameComponentGroup::Graphical),
							_textureId(0),
							_flipped(false),
							_visible(true)
{
	AddType(GameComponentType::Graphical);
}


GcDrawable::~GcDrawable()
{
}

void GcDrawable::Update(float secondsElapsed)
{
	GameObject* go = GetGameObject();
	GameComponent* gc = go->GetComponent(GameComponentGroup::Physical);
	GcPhysical* phys = static_cast<GcPhysical*>(gc);
	b2Vec2 pos = phys->GetPosition();
	_x = pos.x;
	_y = pos.y;
	_angle = phys->GetAngle();
}

unsigned int GcDrawable::GetTextureId() const
{
	return _textureId;
}

void GcDrawable::SetTextureId(unsigned int textureId)
{
	_textureId = textureId;
}

float GcDrawable::GetX() const
{
	return _x;
}

float GcDrawable::GetY() const
{
	return _y;
}

float GcDrawable::GetAngle() const
{
	return _angle;
}

void GcDrawable::FlipX(bool flipped)
{
	_flipped = flipped;
}

bool GcDrawable::IsFlipped() const
{
	return _flipped;
}


void GcDrawable::SetVisibility(bool visible)
{
	_visible = visible;
}
bool GcDrawable::IsVisible() const
{
	return _visible;
}