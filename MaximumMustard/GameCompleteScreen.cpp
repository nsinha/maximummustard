#include "GameCompleteScreen.h"
#include <algorithm>
#include "StartScreen.h"
#include "ScreenManager.h"

using namespace std;

GameCompleteScreen::GameCompleteScreen(sf::RenderWindow* window) : BaseScreen(window)
{
}


GameCompleteScreen::~GameCompleteScreen()
{
}

void GameCompleteScreen::Initialize()
{
	SortFinalScores();
	_textFont.loadFromFile(".\\Assets\\Fonts\\GI-Bold.ttf");
	sf::View view;
	view.setSize(static_cast<float>(_window->getSize().x), static_cast<float>(_window->getSize().y));
	view.setCenter(static_cast<float>(_window->getSize().x) / 2.f, static_cast<float>(_window->getSize().y) / 2.f);
	view.setViewport(sf::FloatRect(0.f, 0.f, 1.f, 1.f));
	_window->setView(view);
}

bool GameCompleteScreen::Update()
{
	for (int i = 0; i < 4; ++i)
	{
		if (!sf::Joystick::isConnected(i))
		{
			continue;
		}

		if (sf::Joystick::isButtonPressed(i, 7))
		{
			StartScreen* startScreen = new StartScreen(_window);
			startScreen->Initialize();
			ScreenManager::GetInstance().SwitchToScreen(startScreen);
			return false;
		}
	}
	return true;
}

void GameCompleteScreen::Draw()
{
	_window->clear();
	float height = 50.f;

	sf::Text headerText;
	headerText.setFont(_textFont);
	headerText.setPosition(100.f, height);
	headerText.setColor(sf::Color::White);
	headerText.setCharacterSize(100);
	headerText.setString("Player");
	_window->draw(headerText);

	headerText.setPosition(750.f, height);
	headerText.setString("Kills");
	_window->draw(headerText);

	headerText.setPosition(1000.f, height);
	headerText.setString("Deaths");
	_window->draw(headerText);

	height += 110;

	for (auto& fs : _finalScores)
	{
		
		sf::Text text;
		text.setFont(_textFont);
		text.setPosition(100.f, height);
		text.setColor(sf::Color::White);
		text.setCharacterSize(75);
		text.setString(fs.PlayerName);
		_window->draw(text);

		text.setPosition(100.f, height);
		text.setString(fs.PlayerName);
		_window->draw(text);

		text.setPosition(750.f, height);
		text.setString(to_string(fs.Kills));
		_window->draw(text);

		text.setPosition(1000.f, height);
		text.setString(to_string(fs.Deaths));
		_window->draw(text);
		height += 80;
	}
	_window->display();
}

bool CompareDeaths(FinalScore fs1, FinalScore fs2)
{
	return fs1.Deaths > fs2.Deaths;
}

bool CompareKills(FinalScore fs1, FinalScore fs2)
{
	return fs1.Kills > fs2.Kills;
}

void GameCompleteScreen::SortFinalScores()
{
	stable_sort(_finalScores.begin(), _finalScores.end(), CompareDeaths);
	stable_sort(_finalScores.begin(), _finalScores.end(), CompareKills);
}

