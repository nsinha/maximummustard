#ifndef GC_PROJECTILE_H
#define GC_PROJECTILE_H

#include "GcPhysical.h"
class GcProjectile :
	public GcPhysical
{
public:
	GcProjectile();
	virtual ~GcProjectile();
	virtual void Update(float secondsElapsed) override;
private:
	int _playerId;
};

#endif