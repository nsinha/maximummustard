#ifndef CONTACT_LISTENER_H
#define CONTACT_LISTENER_H

#include <Box2D/Box2D.h>

class IHitDetector;

class ContactListener : public b2ContactListener
{
public:
	void SetHitDetector(IHitDetector* hitDetector)
	{
		_hitDetector = hitDetector;
	}
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
private:
	IHitDetector *_hitDetector;

};

#endif