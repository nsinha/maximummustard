#include "GcActor.h"

#ifndef GAME_OBJECT_H
	#include "GameObject.h"
#endif

#ifndef GAME_COMPONENT_H
	#include "GameComponent.h"
#endif

#ifndef GAME_PHYSICAL_H
	#include "GcPhysical.h"
#endif

#ifndef GAME_DRAWABLE_H
	#include "GcDrawable.h"
#endif

#ifndef LEVEL_H
	#include "Level.h"
#endif

GcActor::GcActor() : GameComponent(GameComponentGroup::Actor),
					_jumpCooldown(0),
					_shootCooldown(0),
					_pullStrength(0),
					_level(nullptr)
{
	_walkFrame = 0;
	AddType(GameComponentType::Actor);
}


GcActor::~GcActor()
{
}

void GcActor::MoveLeft(float magnitude)
{
	auto physics = GetPhysics();
	std::string currentStateName = _state.GetCurrentStateName();
	if (currentStateName == "Drawn")
	{
		return;
	}
	auto body = physics->GetBody();
	float velChange = -8.f * magnitude/100.f - body->GetLinearVelocity().x;;
	float impulse = body->GetMass() * velChange;
	body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
	_state.GoToState("Walking");
}

void GcActor::MoveRight(float magnitude)
{
	auto physics = GetPhysics();
	std::string currentStateName = _state.GetCurrentStateName();
	if (currentStateName == "Drawn")
	{
		return;
	}
	auto body = physics->GetBody();
	float velChange = 8.f * magnitude/100.f-body->GetLinearVelocity().x;;
	float impulse = body->GetMass() * velChange;
	body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
	_state.GoToState("Walking");
}

void GcActor::Jump()
{
	auto physics = GetPhysics();
	if (!physics->IsOnGround() || _jumpCooldown > 0)
	{
		return;
	}
	if (_state.GoToState("Jumping"))
	{
		_jumpCooldown = 30;
		auto body = physics->GetBody();
		body->ApplyForce(b2Vec2(0.f, -1800.f), body->GetWorldCenter(), true);
	}
}

GcPhysical* GcActor::GetPhysics()
{
	auto go = GetGameObject();
	GameComponent* gc = go->GetComponent(GameComponentGroup::Physical);
	GcPhysical* physical = static_cast<GcPhysical*>(gc);
	return physical;

}

void GcActor::SetPullStrength(float pullStrength)
{
	_pullStrength = pullStrength;
	_state.GoToState("Drawn");
}

void GcActor::CancelPull()
{
	_pullStrength = 0.0f;
	_state.GoToState("Standing");
}

void GcActor::Shoot()
{
	if (_shootCooldown > 0)
	{
		return;
	}
	auto state = _state.GetCurrentStateName();
	if (state != "Drawn")
	{
		return;
	}
	_shootCooldown = 60;

	auto go = GetGameObject();
	auto physics = GetPhysics();
	b2Vec2 position = physics->GetPosition();
	GameComponent* gc = go->GetComponent(GameComponentGroup::Graphical);
	GcDrawable* drawable = static_cast<GcDrawable*>(gc); 
	float xOffset = 8.5f / 32.f;
	float yOffset = 3.f / 32.f;
	float pullStrength = _pullStrength;
	if (drawable->IsFlipped())
	{
		xOffset *= -1.f;
		pullStrength *= -1.f;
	}

	_level->GenerateArrow(go, position.x + xOffset, position.y + yOffset, pullStrength, 0.f, pullStrength);
	CancelPull();
}

void GcActor::SetLeftBowVisiblity(bool visible)
{
	GameObject* go = GetGameObject();
	GameObject* leftBow = go->GetChild("leftBow");
	if (leftBow != nullptr)
	{
		GameComponent *gc = leftBow->GetComponent(GameComponentGroup::Graphical);
		if (gc != nullptr)
		{
			GcDrawable* drawable = static_cast<GcDrawable*>(gc);
			drawable->SetVisibility(visible);
		}
	}
}

void GcActor::SetRightBowVisiblity(bool visible)
{
	GameObject* go = GetGameObject();
	GameObject* rightBow = go->GetChild("rightBow");
	if (rightBow != nullptr)
	{
		GameComponent *gc = rightBow->GetComponent(GameComponentGroup::Graphical);
		if (gc != nullptr)
		{
			GcDrawable* drawable = static_cast<GcDrawable*>(gc);
			drawable->SetVisibility(visible);
		}
	}
}

void GcActor::Update(float secondsElapsed)
{
	if (_jumpCooldown > 0)
	{
		--_jumpCooldown;
	}

	if (_shootCooldown > 0)
	{
		--_shootCooldown;
	}

	_state.Update(secondsElapsed);
	std::string stateName = _state.GetCurrentStateName();

	auto go = GetGameObject();
	GameComponent* gc = go->GetComponent(GameComponentGroup::Graphical);
	GcDrawable* drawable = static_cast<GcDrawable*>(gc);
	auto physics = GetPhysics();
	if (stateName == "Walking")
	{
		++_walkFrame;
		
		auto body = physics->GetBody();
		float velX = body->GetLinearVelocity().x;
		
		SetLeftBowVisiblity(false);
		SetRightBowVisiblity(false);
		if (velX > 0.0f)
		{
			drawable->FlipX(false);
		}
		else
		{
			drawable->FlipX(true);
		}
	}
	else if (stateName == "Standing")
	{
		SetLeftBowVisiblity(false);
		SetRightBowVisiblity(false);
		_walkFrame = 0;
		if (!physics->IsOnGround())
		{
			return;
		}
		auto body = physics->GetBody();
		float velChange = -body->GetLinearVelocity().x;
		float impulse = body->GetMass() * velChange;
		body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
	}
	else if (stateName == "Jumping")
	{
		_walkFrame = 10;
		if (_jumpCooldown < 20 && physics->IsOnGround())
		{
			_state.GoToState("Standing");
		}
	}
	else if (stateName == "Drawn")
	{
		_walkFrame = 10;
		auto body = physics->GetBody();
		if (physics->IsOnGround())
		{
			float velChange = -body->GetLinearVelocity().x;
			float impulse = body->GetMass() * velChange;
			body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
		}

		GameObject* leftBow = go->GetChild("leftBow");
		GameObject* rightBow = go->GetChild("rightBow");
		if (!drawable->IsFlipped())
		{
			SetRightBowVisiblity(true);
		}
		else
		{
			SetLeftBowVisiblity(true);
		}
		
	}
	
	drawable->SetTextureId((_walkFrame / 5 % 4) + 11);
}

void GcActor::IncreasePullStrength()
{
	_pullStrength += 0.75f;
	if (_pullStrength > 90.f)
	{
		_pullStrength = 90.f;
	}
	_state.GoToState("Drawn");
}

void GcActor::SetLevel(Level* level)
{
	_level = level;
}

bool GcActor::IsDrawn() const
{
	std::string stateName = _state.GetCurrentStateName();
	return stateName == "Drawn";
}