#include "StartScreen.h"
#include <vector>
#include <SFML/Graphics.hpp>
#include "GameScreen.h"
#include "ScreenManager.h"

using namespace std;

StartScreen::StartScreen(sf::RenderWindow* window) : BaseScreen(window)
{
}

void StartScreen::Initialize()
{
	for (int i = 0; i < 4; ++i)
	{
		_views[i].setSize(static_cast<float>(_window->getSize().x), static_cast<float>(_window->getSize().y));
		_views[i].setCenter(static_cast<float>(_window->getSize().x) / 2.f, static_cast<float>(_window->getSize().y) / 2.f);
	}
	
	_views[0].setViewport(sf::FloatRect(0.f, 0.f, 0.5f, 0.5f));
	_views[1].setViewport(sf::FloatRect(0.5f, 0.f, 0.5f, 0.5f));
	_views[2].setViewport(sf::FloatRect(0.f, 0.5f, 0.5f, 0.5f));
	_views[3].setViewport(sf::FloatRect(0.5f, 0.5f, 0.5f, 0.5f));

	for (int i = 0; i < 4; ++i)
	{
		_playerStates[i] = PlayerState::Inactive;
	}

	_pressAToJoinTex.loadFromFile(".\\Assets\\Images\\PressAToJoin.png");
	_pressAToJoin.setTexture(_pressAToJoinTex);

	_playerTex.loadFromFile(".\\Assets\\Images\\PlayerActive.png");
	_playerSprite.setTexture(_playerTex);

	_readyTex.loadFromFile(".\\Assets\\Images\\WaitingForOtherPlayers.png");
	_readySprite.setTexture(_readyTex);
}

bool StartScreen::Update()
{
	for (int i = 0; i < 4; ++i)
	{
		if (!sf::Joystick::isConnected(i))
		{
			continue;
		}

		if (_playerStates[i] == PlayerState::Inactive)
		{
			//Button A
			if (sf::Joystick::isButtonPressed(i, 0))
			{
				_playerStates[i] = PlayerState::Active;
			}
		}
		else if (_playerStates[i] == PlayerState::Active)
		{
			//Button B
			if (sf::Joystick::isButtonPressed(i, 1))
			{
				_playerStates[i] = PlayerState::Inactive;
			}

			//Button Start
			if (sf::Joystick::isButtonPressed(i, 7))
			{
				_playerStates[i] = PlayerState::Ready;
			}
		}
		else if (_playerStates[i] == PlayerState::Ready)
		{
			//Button Back
			if (sf::Joystick::isButtonPressed(i, 6))
			{
				_playerStates[i] = PlayerState::Active;
			}
		}
	}
	if (GetNumberReady() > 0 && GetNumberActive() == 0)
	{
		vector<int> players;
		for (int i = 0; i < 4; ++i)
		{
			if (_playerStates[i] == PlayerState::Ready)
			{
				players.push_back(i);
			}
		}

		GameScreen* screen = new GameScreen(_window, players);
		screen->Initialize();
		ScreenManager::GetInstance().SwitchToScreen(screen);
		return false;
	}
	return true;
}

void StartScreen::Draw()
{
	_window->clear();
	for (int i = 0; i < 4; ++i)
	{
		_window->setView(_views[i]);
		if (_playerStates[i] == PlayerState::Inactive)
		{
			_window->draw(_pressAToJoin);
		}
		else if (_playerStates[i] == PlayerState::Active)
		{
			_window->draw(_playerSprite);
		}
		else if (_playerStates[i] == PlayerState::Ready)
		{
			_window->draw(_readySprite);
		}

	}

	_window->display();
}


int StartScreen::GetNumberActive() const
{
	int count = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (_playerStates[i] == PlayerState::Active)
		{
			++count;
		}
	}
	return count;
}

int StartScreen::GetNumberReady() const
{
	int count = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (_playerStates[i] == PlayerState::Ready)
		{
			++count;
		}
	}
	return count;
}

StartScreen::~StartScreen()
{
}
