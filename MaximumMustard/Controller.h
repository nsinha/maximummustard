#ifndef CONTROLLER_H
#define CONTROLLER_H

class GcActor;

class Controller
{
public:
	Controller(GcActor* actor, int controllerId);
	~Controller();
	void ChangeActor(GcActor* actor);
	void Update(float secondsElapsed);
	int GetControllerId() const
	{
		return _controllerId;
	};
private:
	GcActor* _actor;
	int _controllerId;
};

#endif