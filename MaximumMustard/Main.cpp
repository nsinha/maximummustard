#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <iostream>
#include <map>
#include "Level.h"
#include "GameObject.h"
#include "GcDrawable.h"
#include "GcPhysical.h"
#include "GcActor.h"
#include "Controller.h"
#include "ContactListener.h"
#include <SFML/OpenGL.hpp>
#include "DebugDraw.h"
#include "StartScreen.h"
#include "ScreenManager.h"

int main()
{
	sf::RenderWindow window(sf::VideoMode(1366, 768), "Maximum Mustard");
	window.setVerticalSyncEnabled(true);
	sf::Clock clock;
	sf::View view;
	view.setSize(1366.f, 768.f);
	BaseScreen *currentScreen = new StartScreen(&window);
	ScreenManager::GetInstance().SwitchToScreen(currentScreen);
	currentScreen->Initialize();
	/*Level* l = CreateLevel();
	DebugDraw* draw = new DebugDraw(window);
	draw->SetFlags(b2Draw::e_shapeBit);
	l->GetWorld()->SetDebugDraw(draw);*/
	bool debug = false;
	while (window.isOpen())
	{
		auto elapsed = clock.restart();
		sf::Event event;
		currentScreen = ScreenManager::GetInstance().GetCurrentScreen();
		while (window.pollEvent(event))
		{
			
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
				}
				
				if (event.key.code == sf::Keyboard::Tab)
				{
					debug = true;
				}
			}
			else if (event.type == sf::Event::KeyReleased)
			{
				if (event.key.code == sf::Keyboard::Tab)
				{
					debug = false;
				}
			}
		}

		bool screenLives = currentScreen->Update();
		if (!screenLives)
		{
			delete currentScreen;
			currentScreen = nullptr;
			continue;
		}
		currentScreen->Draw();
		
		/*l->Step();
		window.clear();
		view.setCenter(playerBody->GetPosition().x * 32.f, 768.f / 2.f);
		window.setView(view);
		if (!debug)
		{


			auto gcs = l->GetGameObjects(GameComponentGroup::Graphical);
			TextureManager<sf::Sprite>* textureManager = l->GetTextureManager();
			for (auto gc : gcs)
			{
				GcDrawable* graphical = static_cast<GcDrawable*>(gc);
				if (!graphical->IsVisible())
				{
					continue;
				}
				sf::Sprite *sprite = textureManager->Get(graphical->GetTextureId());
				if (graphical->IsFlipped())
				{
					sprite->setScale(-1.f, 1.f);
				}
				else
				{
					sprite->setScale(1.f, 1.f);
				}
				sf::Vector2f vec(graphical->GetX() * 32.f, graphical->GetY() * 32.f);
				sprite->setPosition(vec);
				window.draw(*sprite);
			}
		}
		else
		{
			l->GetWorld()->DrawDebugData();
		}

		window.display();*/
	}
	return 0;
}

