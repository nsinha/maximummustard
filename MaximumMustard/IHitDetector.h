#ifndef I_HIT_DETECTOR
#define I_HIT_DETECTOR

class GcPhysical;

class IHitDetector
{
public:
	IHitDetector();
	virtual void HandleHit(GcPhysical* hitter, GcPhysical* hittee) = 0;
	virtual ~IHitDetector();
};

#endif