#include "Controller.h"
#include "GcActor.h"
#include <SFML/Graphics.hpp>
#include<iostream>

Controller::Controller(GcActor *actor, int controllerId)
	: _actor{ actor }, _controllerId{ controllerId }
{
}


Controller::~Controller()
{
}

void Controller::ChangeActor(GcActor* actor)
{
	_actor = actor;
}

void Controller::Update(float secondsElapsed)
{
	if (!sf::Joystick::isConnected(_controllerId))
	{
		return;
	}

	if (sf::Joystick::isButtonPressed(_controllerId, 0))
	{
		_actor->Jump();
	}

	if (sf::Joystick::isButtonPressed(_controllerId, 9))
	{
		_actor->Shoot();
	}

	float dir = sf::Joystick::getAxisPosition(_controllerId, sf::Joystick::Axis::X);
	if (dir > 15.f)
	{
		_actor->MoveRight(dir);
	}
	else if (dir < -15.f)
	{
		_actor->MoveLeft(-dir);
	}


	if (sf::Joystick::isButtonPressed(_controllerId, 2))
	{
		_actor->IncreasePullStrength();
	}
	else if (_actor->IsDrawn())
	{
		_actor->Shoot();
	}

	/*float pullStrength = sf::Joystick::getAxisPosition(_controllerId, sf::Joystick::Axis::Z);

	if (pullStrength < -1.0f)
	{
		_actor->SetPullStrength(-pullStrength);
	}
	else
	{
		_actor->CancelPull();
	}*/
}