#include "StateMachine.h"


StateMachine::StateMachine()
{
	_rootState = new State();
	_rootState->TotalTime = 1.f / 60.f;
	_rootState->TimeLeft = _rootState->TotalTime;
	_rootState->States["Default"] = _rootState;
	_rootState->StateName = "Standing";
	_rootState->Locked = false;

	State* walking = new State();
	walking->TotalTime = 8.f / 60.f;
	walking->TimeLeft = _rootState->TotalTime;
	walking->StateName = "Walking";
	walking->Locked = false;
	_rootState->States["Walking"] = walking;
	walking->States["Default"] = _rootState;
	walking->States["Walking"] = walking;

	State* jumping = new State();
	jumping->TotalTime = 0.f;
	jumping->TimeLeft = 0.f;
	jumping->StateName = "Jumping";
	jumping->Locked = true;
	jumping->States["Standing"] = _rootState;
	jumping->States["Walking"] = walking;

	walking->States["Jumping"] = jumping;
	_rootState->States["Jumping"] = jumping;

	State* drawn = new State();
	drawn->TotalTime = 1.f / 60.f;
	drawn->TimeLeft = drawn->TotalTime;
	drawn->StateName = "Drawn";

	drawn->States["Default"] = drawn;
	drawn->States["Standing"] = _rootState;


	_rootState->States["Drawn"] = drawn;
	walking->States["Drawn"] = drawn;

	_currentState = _rootState;
}


StateMachine::~StateMachine()
{
}


void StateMachine::Update(float secondsElapsed)
{
	if (_currentState->Locked)
	{
		return;
	}
	_currentState->TimeLeft -= secondsElapsed;
	if (_currentState->TimeLeft <= 0.f)
	{
		_currentState = _currentState->States["Default"];
		_currentState->TimeLeft = _currentState->TotalTime;

	}
}

bool StateMachine::GoToState(std::string stateName)
{
	auto found = _currentState->States.find(stateName);
	if (found == _currentState->States.end())
	{
		return false;
	}
	_currentState = found->second;
	_currentState->TimeLeft = _currentState->TotalTime;
	return true;
}

std::string StateMachine::GetCurrentStateName() const
{
	return _currentState->StateName;
}