#ifndef GAME_COMPONENT_H
#define GAME_COMPONENT_H

class GameObject;

enum class GameComponentGroup
{
	Physical,
	Graphical,
	Actor,
	Damageable
};

enum GameComponentType
{
	Missing = 0,
	Physical=1,
	Graphical=1<<1,
	Actor=1<<2,
	Projectile=1<<3,
	Damageable=1<<4,
};

class GameComponent
{
public:
	GameComponent(GameComponentGroup gcGroup);
	virtual ~GameComponent();
	GameComponentGroup GetComponentGroup() const;
	void SetGameObject(GameObject* go);
	virtual void Update(float secondsElapsed)=0;
	GameObject* GetGameObject() const;
	bool IsOfType(GameComponentType gcType) const;
protected:
	void AddType(GameComponentType gcType);
private:
	GameComponentGroup _gcGroup;
	GameObject* _go;
	GameComponentType _gcType;
};

#endif