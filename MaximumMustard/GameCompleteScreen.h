#ifndef GAME_COMPLETE_SCREEN
#define GAME_COMPLETE SCREEN

#include "BaseScreen.h"

struct FinalScore
{
	std::string PlayerName;
	int Kills;
	int Deaths;
};

class GameCompleteScreen :
	public BaseScreen
{
public:
	GameCompleteScreen(sf::RenderWindow* window);
	virtual ~GameCompleteScreen();
	void SetFinalScores(std::vector<FinalScore> finalScores)
	{
		_finalScores = finalScores;
	}
	virtual void Initialize() override;
	virtual bool Update() override;
	virtual void Draw() override;
private:
	void SortFinalScores();
	std::vector<FinalScore> _finalScores;
	sf::Font _textFont;
};

#endif