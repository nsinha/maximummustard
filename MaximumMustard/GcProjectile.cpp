#include "GcProjectile.h"


GcProjectile::GcProjectile() : _playerId(-1)
{
	AddType(GameComponentType::Projectile);
}



GcProjectile::~GcProjectile()
{
}

void GcProjectile::Update(float secondsElapsed)
{
	float dragConstant = .3f;
	auto arrowBody = this->GetBody();
	b2Vec2 pointingDirection = arrowBody->GetWorldVector(b2Vec2(1, 0));
	b2Vec2 flightDirection = arrowBody->GetLinearVelocity();
	float flightSpeed = flightDirection.Normalize();//normalizes and returns length

	float dot = b2Dot(flightDirection, pointingDirection);
	float dragForceMagnitude = (1 - fabs(dot)) * flightSpeed * flightSpeed * dragConstant * arrowBody->GetMass();
	float width = 0.46875f / 4.f;
	float endPoint = -width * 3.f;
	b2Vec2 arrowTailPosition = arrowBody->GetWorldPoint(b2Vec2(endPoint, 0));
	arrowBody->ApplyForce(dragForceMagnitude * -flightDirection, arrowTailPosition, true);
}