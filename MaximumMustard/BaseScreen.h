#ifndef Base_SCREEN_H
#define Base_SCREEN_H

#include <SFML/Graphics.hpp>

class BaseScreen
{
public:
	BaseScreen(sf::RenderWindow* window);
	virtual void Initialize() = 0;
	virtual bool Update() = 0;
	virtual void Draw() = 0;
	virtual ~BaseScreen();
protected:
	sf::RenderWindow* _window;
};

#endif