#ifndef CAMERA_H
#define CAMERA_H

class GameObject;

class Camera 
{
public:
	Camera(float width, float height);
	void SetTarget(GameObject* target);
	virtual void Update(float secondsElapsed);
	void SnapToTarget();
	virtual ~Camera();

	float GetWidth() const
	{
		return _width;
	}

	float GetHeight() const
	{
		return _height;
	}

	float GetCenterX() const
	{
		return _centerX;
	}

	float GetCenterY() const
	{
		return _centerY;
	}

private:
	GameObject* _target;
	float _width, _height;
	float _centerX, _centerY;
	float _targetX, _targetY;
};

#endif