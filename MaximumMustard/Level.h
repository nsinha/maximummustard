#ifndef LEVEL_H
#define LEVEL_H

#include<list>
#include <SFML/Graphics.hpp>

#ifndef GAME_COMPONENT_H
	#include "GameComponent.h"
#endif

#ifndef TEXTURE_MANAGER_H
	#include "TextureManager.h"
#endif


#ifndef I_HIT_DETECTOR_H
	#include "IHitDetector.h"
#endif

#include<Box2D/Box2D.h>

class GcDamageable;
class GcProjectile;
class Controller;
class GameObject;
class GcPhysical;
class IKillHandler;
class IGameManager;

class Level : public IHitDetector
{
public:
	Level();
	~Level();
	void Add(GameObject* go);
	void Step();
	std::list<GameComponent*> GetGameObjects(GameComponentGroup gcGroup) const;
	void AddController(Controller* controller);
	b2World* GetWorld();
	void LoadLevel(std::string filename);
	TextureManager<sf::Sprite>* GetTextureManager() const;
	void WeldJoint(GcPhysical* parent, GcPhysical* child, float localX, float localY);
	void GenerateArrow(GameObject *owner, float x, float y, float dx, float dy, float velocity);
	GameObject* GeneratePlayer(int controllerId);
	void SetKillHandler(IKillHandler* killHandler);
	void SetGameManager(IGameManager* gameManager){ _gameManager = gameManager; }
	virtual void HandleHit(GcPhysical* hitter, GcPhysical* hittee) override;
	
private:
	b2Vec2 GetBestStartPosition() const;
	std::list<GameObject*> _gos;
	std::list<Controller*> _controllers;
	std::list<GameObject*> _toRemove;
	std::list<GameObject*> _characters;
	std::list<int> _toRegen;
	std::vector<b2Vec2> _startPositions;
	b2World* _world;
	TextureManager<sf::Sprite>* _textureManager;
	IKillHandler* _killHandler;
	IGameManager* _gameManager;
	void RecurseGameObjects(GameObject* go, GameComponentGroup gcGroup, std::list<GameComponent*> &toReturn) const;
	void ApplyDamage(GcDamageable* damageable, GcProjectile* projectile);
	void CleanUp(GameObject* go);
	bool MarkForCleanup(GameObject* go);
	const float pixelsPerMeter = 32.f;

};

#endif 