#ifndef START_SCREEN_H
#define START_SCREEN_H

#ifndef GAME_SCREEN_H
#include "BaseScreen.h"
#endif

enum class PlayerState
{
	Inactive,
	Active,
	Ready
};

class StartScreen :
	public BaseScreen
{
public:
	StartScreen(sf::RenderWindow* window);
	virtual void Initialize();
	virtual bool Update();
	virtual void Draw();
	virtual ~StartScreen();
private:

	int GetNumberReady() const;
	int GetNumberActive() const;

	sf::View _views[4];
	PlayerState _playerStates[4];
	
	sf::Sprite _pressAToJoin;
	sf::Texture _pressAToJoinTex;

	sf::Sprite _playerSprite;
	sf::Texture _playerTex;

	sf::Sprite _readySprite;
	sf::Texture _readyTex;

};

#endif