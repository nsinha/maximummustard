#include "Camera.h"
#include "GameObject.h"
#include "GcPhysical.h"
#include <math.h>

Camera::Camera(float width, float height) : _width(width),
												_height(height),
												_centerX(width/2.f),
												_centerY(height/2.f),
												_targetX(_centerX),
												_targetY(_centerY),
												_target(nullptr)
{

}

void Camera::SetTarget(GameObject* target)
{
	_target = target;
}

Camera::~Camera()
{
}

void Camera::SnapToTarget()
{
	_centerX = _targetX;
	_centerY = _targetY;
}

void Camera::Update(float secondsElapsed)
{
	if (_target == nullptr)
	{
		return;
	}
	GameComponent* gc = _target->GetComponent(GameComponentGroup::Physical);
	GcPhysical* phys = dynamic_cast<GcPhysical*>(gc);
	if (phys == nullptr)
	{
		return;
	}

	b2Vec2 pos = phys->GetPosition();
	float halfWidth = _width / 150.f;
	float halfHeight = _height / 150.f;
	
	if (pos.x < _targetX - halfWidth
		|| pos.x > _targetX + halfWidth)
	{
		_targetX = pos.x;
	}

	if (pos.y < _targetY - halfHeight
		|| pos.y > _targetY + halfHeight)
	{
		_targetY = pos.y;
	}
	
	float diff = _targetX - _centerX;
	if (fabs(diff) > 0.001)
	{
		_centerX += (diff * secondsElapsed);
	}

	diff = _targetY - _centerY;
	if (fabs(diff) > 0.001)
	{
		_centerY += (diff * secondsElapsed);
	}
}
