#include "GameObject.h"
#include<iostream>

unsigned int GameObject::GetUniqueId()
{
	static int firstId = 0;
	return firstId++;
}

GameObject::GameObject()
{
	_id = GetUniqueId();
}


GameObject::~GameObject()
{
	for (auto goc : _gcs)
	{
		delete goc.second;
	}

	//DO NOT DELETE CHILDREN.  THAT IS DONE IN LEVEL::CLEANUP
}

unsigned int GameObject::GetId() const
{
	return _id;
}

void GameObject::AddComponent(GameComponent* gc)
{
	gc->SetGameObject(this);
	_gcs[gc->GetComponentGroup()] = gc;
}

GameComponent* GameObject::GetComponent(GameComponentGroup gcGroup) const
{
	auto found = _gcs.find(gcGroup);
	if (found == _gcs.end())
	{
		return nullptr;
	}
	return found->second;
}

void GameObject::Update(float secondsElapsed)
{
	for (auto it = _gcs.begin(); it != _gcs.end(); ++it)
	{
		it->second->Update(secondsElapsed);
	}

	for (auto it = _children.begin(); it != _children.end(); ++it)
	{
		it->second->Update(secondsElapsed);
	}
}

void GameObject::AddChild(std::string childName, GameObject* go)
{
	_children[childName] = go;
}
std::list<GameObject*> GameObject::GetChildren()
{
	std::list<GameObject*> toReturn;
	for (auto it = _children.begin(); it != _children.end(); ++it)
	{
		toReturn.push_back(it->second);
	}
	return toReturn;
}

GameObject* GameObject::GetChild(std::string name)
{
	auto found = _children.find(name);
	if (found == _children.end())
	{
		return nullptr;
	}
	return found->second;
}

void GameObject::SetOwner(int playerId)
{
	_ownerId = playerId;
}
int GameObject::GetOwner() const
{
	return _ownerId;
}