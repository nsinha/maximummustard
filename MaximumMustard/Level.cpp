#define _CRT_SECURE_NO_DEPRECATE
#include "Level.h"

#include <Box2D/Box2D.h>

#ifndef GAME_OBJECT_H
#include "GameObject.h"
#endif

#ifndef CONTROLLER_H
#include "Controller.h"
#endif

#ifndef GC_PHYSICAL_H
#include "GcPhysical.h"
#endif

#ifndef GC_PROJECTILE_H
#include "GcProjectile.h"
#endif

#ifndef GC_DRAWABLE_H
#include "GcDrawable.h"
#endif

#ifndef GC_ACTOR_H
#include "GcActor.h"
#endif

#ifndef I_KILL_HANDLER_H
#include "IKillHandler.h"
#endif

#include "IGameManager.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"   // FileReadStream
#include <cstdio>
#include <iostream>
#include "GcDamageable.h"
#include "Camera.h"

#include <algorithm>    // std::find

using namespace std;
using namespace rapidjson;

Level::Level() : _killHandler(nullptr), _gameManager(nullptr)
{
	b2Vec2 gravity(0.0f, 9.8f);
	_world = new b2World(gravity);
	_textureManager = new TextureManager<sf::Sprite>();
}


Level::~Level()
{
	delete _world;
}

void Level::Step()
{
	_world->Step(1.0f / 60.0f, 8, 3);
	for (auto go : _gos)
	{
		go->Update(1.0f / 60.0f);
	}

	for (auto con : _controllers)
	{
		con->Update(1.0f / 60.0f);
	}
	for (auto go : _toRemove)
	{
		_gos.remove(go);
		CleanUp(go);
		_characters.remove(go);
	}
	_toRemove.clear();

	for (int playerId : _toRegen)
	{
		GameObject* newPlayer = GeneratePlayer(playerId);
		for (auto controller : _controllers)
		{
			if (controller->GetControllerId() != playerId)
			{
				continue;
			}
			GameComponent* gc = newPlayer->GetComponent(GameComponentGroup::Actor);
			GcActor *actor = dynamic_cast<GcActor*>(gc);
			controller->ChangeActor(actor);
			break;
		}
		if (_gameManager != nullptr)
		{

			_gameManager->ChangePlayerTarget(playerId, newPlayer);
		}
	}
	_toRegen.clear();
}

void Level::CleanUp(GameObject *go)
{
	GcPhysical* physical = dynamic_cast<GcPhysical*>(go->GetComponent(GameComponentGroup::Physical));
	if (physical != nullptr)
	{
		_world->DestroyBody(physical->GetBody());
	}
	for (auto child : go->GetChildren())
	{
		CleanUp(child);
	}
	delete go;
}

void Level::Add(GameObject* go)
{
	_gos.push_back(go);
}

std::list<GameComponent*> Level::GetGameObjects(GameComponentGroup gcGroup) const
{
	std::list<GameComponent*> toReturn;
	for (auto it = _gos.begin(); it != _gos.end(); ++it)
	{
		RecurseGameObjects(*it, gcGroup, toReturn);
	}
	return toReturn;
}

void Level::RecurseGameObjects(GameObject* go, GameComponentGroup gcGroup, std::list<GameComponent*> &toReturn) const
{
	GameComponent* gc = go->GetComponent(gcGroup);
	if (gc)
	{
		toReturn.push_back(gc);
	}
	std::list<GameObject*> children = go->GetChildren();
	for (auto it = children.begin(); it != children.end(); ++it)
	{
		RecurseGameObjects(*it, gcGroup, toReturn);
	}
}

b2World* Level::GetWorld()
{
	return _world;
}

void Level::AddController(Controller* controller)
{
	_controllers.push_back(controller);
}

void Level::LoadLevel(std::string filename)
{
	_textureManager->Clear();
	FILE* fp = fopen(filename.c_str(), "r"); // non-Windows use "r"
	char readBuffer[65536];
	FileReadStream bis(fp, readBuffer, sizeof(readBuffer));
	Document d; // Document is GenericDocument<UTF8<> > 
	std::cout << d.ParseStream<0, UTF8<>, FileReadStream>(bis).HasParseError() << std::endl;  // Parses UTF-16LE file into UTF-8 in memory
	std::cout << d.GetParseError() << std::endl;
	fclose(fp);

	Value &textures = d["Textures"];
	for (SizeType i = 0; i < textures.Size(); ++i)
	{
		Value &val = textures[i];
		auto str = val.GetString();
		sf::Texture* tex = new sf::Texture();
		tex->loadFromFile(str);
		sf::Sprite* sprite = new sf::Sprite();
		sprite->setTexture(*tex);
		sf::IntRect rect = sprite->getTextureRect();
		sprite->setOrigin(rect.width / 2, rect.height / 2);
		_textureManager->Add(i, sprite);
	}


	Value &world = d["World"];
	for (SizeType i = 0; i < world.Size(); ++i)
	{
		Value &part = world[i];
		float x = part["x"].GetDouble();
		float y = part["y"].GetDouble();
		float width = part["width"].GetDouble();
		float height = part["height"].GetDouble();
		int tex = part["tex"].GetInt();

		GameObject* go = new GameObject();
		Add(go);
		GcPhysical* phys = new GcPhysical();
		phys->InitializeShapes(x, y, width, height, true, _world, true);
		go->AddComponent(phys);

		GcDrawable* drawable = new GcDrawable();
		drawable->SetTextureId(tex);
		go->AddComponent(drawable);
	}

	_startPositions.clear();

	Value &start = d["StartLocations"];
	for (SizeType i = 0; i < start.Size(); ++i)
	{
		Value &part = start[i];
		float x = part["x"].GetDouble();
		float y = part["y"].GetDouble();
		b2Vec2 pos(x, y);
		_startPositions.push_back(pos);
	}
}


TextureManager<sf::Sprite>* Level::GetTextureManager() const
{
	return _textureManager;
}

void Level::WeldJoint(GcPhysical* parent, GcPhysical* child, float localX, float localY)
{
	b2WeldJointDef weldJointDef;
	weldJointDef.bodyA = parent->GetBody();
	weldJointDef.bodyB = child->GetBody();
	b2Vec2 contact(localX, localY);
	b2Vec2 zero(0.f, 0.f);
	weldJointDef.localAnchorA = contact;
	weldJointDef.localAnchorB = zero;
	_world->CreateJoint(&weldJointDef);
}

void Level::GenerateArrow(GameObject *owner, float x, float y, float dx, float dy, float velocity)
{
	GameObject* arrow = new GameObject();
	arrow->SetOwner(owner->GetOwner());
	GcProjectile* physical = new GcProjectile();
	physical->CreateArrow(_world, x, y, dx, dy, velocity);
	arrow->AddComponent(physical);
	GcDrawable* drawable = new GcDrawable();
	drawable->SetTextureId(16);
	if (velocity < 0.f)
	{
		drawable->FlipX(true);
	}
	arrow->AddComponent(drawable);
	Add(arrow);
}

b2Vec2 Level::GetBestStartPosition() const
{
	int length =_startPositions.size();
	
	int selectedStart = 0;
	int numCharacters = _characters.size();
	float currentMax = 0.f;
	for (int i = 0; i < length; ++i)
	{
		b2Vec2 currentPos = _startPositions[i];
		int minDistance = 200;
		for (auto character : _characters)
		{
			GameComponent *gc = character->GetComponent(GameComponentGroup::Physical);
			GcPhysical *phys = static_cast<GcPhysical*>(gc);
			b2Vec2 pos = phys->GetPosition();
			float distance = b2Distance(currentPos, pos);
			if (distance < minDistance)
			{
				minDistance = distance;
			}
		}

		if (minDistance > currentMax)
		{
			selectedStart = i;
			currentMax = minDistance;
		}
	}
	return _startPositions[selectedStart];
}

void Level::HandleHit(GcPhysical* phys1, GcPhysical* phys2)
{
	GameObject* go2 = phys2->GetGameObject();
	GameObject* go1 = phys1->GetGameObject();
	if (go1->GetOwner() == go2->GetOwner())
	{
		return;
	}

	if (phys1->IsOfType(GameComponentType::Projectile))
	{
		GcDamageable* damageable = dynamic_cast<GcDamageable*>(phys2->GetGameObject()->GetComponent(GameComponentGroup::Damageable));
		if (damageable == nullptr)
		{
			return;
		}
		GcProjectile* projectile = dynamic_cast<GcProjectile*>(phys1);
		ApplyDamage(damageable, projectile);
	}
	else if (phys2->IsOfType(GameComponentType::Projectile))
	{
		GcDamageable* damageable = dynamic_cast<GcDamageable*>(phys1->GetGameObject()->GetComponent(GameComponentGroup::Damageable));
		if (damageable == nullptr)
		{
			return;
		}
		GcProjectile* projectile = dynamic_cast<GcProjectile*>(phys2);
		ApplyDamage(damageable, projectile);
	}
}

void Level::ApplyDamage(GcDamageable* damageable, GcProjectile* projectile)
{
	std::cout << "Hit" << std::endl;
	bool killed = damageable->TakeDamage(1);
	if (killed)
	{
		int killerId = projectile->GetGameObject()->GetOwner();
		GameObject* killee = damageable->GetGameObject();
		int killeeId = killee->GetOwner();
		if (_killHandler != nullptr)
		{
			_killHandler->HandleKill(killerId, killeeId);
		}
		if (MarkForCleanup(killee))
		{
			_toRegen.push_back(killeeId);
		}
		
	}
	MarkForCleanup(projectile->GetGameObject());
}

bool Level::MarkForCleanup(GameObject* go)
{
	auto found = find(_toRemove.begin(), _toRemove.end(), go);
	if (found != _toRemove.end())
	{
		return false;
	}
	_toRemove.push_back(go);
	return true;
}


GameObject* Level::GeneratePlayer(int controllerId)
{
	GameObject *player = new GameObject();
	player->SetOwner(controllerId);
	Add(player);

	GcDamageable* damageable = new GcDamageable(3);
	player->AddComponent(damageable);

	GcPhysical* playerPhys = new GcPhysical();

	/*b2Vec2 pos = _startPositions[controllerId];*/
	b2Vec2 pos = GetBestStartPosition();

	playerPhys->InitializeShapes(pos.x,
		pos.y,
		32.f / pixelsPerMeter,
		64.f / pixelsPerMeter,
		false, _world, true);
	player->AddComponent(playerPhys);
	playerPhys->AddBottomSensor();

	GcDrawable* playerDrawable = new GcDrawable();
	playerDrawable->SetTextureId(11);
	player->AddComponent(playerDrawable);

	GcActor* actor = new GcActor();
	player->AddComponent(actor);
	actor->SetLevel(this);

	GameObject* rightBow = new GameObject();
	GcDrawable* rightBowDrawable = new GcDrawable();
	rightBowDrawable->SetTextureId(15);
	rightBowDrawable->SetVisibility(false);
	rightBow->AddComponent(rightBowDrawable);

	GcPhysical *rightBowPhys = new GcPhysical();
	rightBowPhys->InitializeShapes(48.f / pixelsPerMeter,
		600.f / pixelsPerMeter,
		13.f / pixelsPerMeter,
		24.f / pixelsPerMeter,
		false, _world, false);

	rightBow->AddComponent(rightBowPhys);

	player->AddChild("rightBow", rightBow);

	WeldJoint(playerPhys, rightBowPhys, 8.5f / pixelsPerMeter, 3.f / pixelsPerMeter);

	GameObject* leftBow = new GameObject();
	GcDrawable* leftBowDrawable = new GcDrawable();
	leftBowDrawable->SetTextureId(15);
	leftBowDrawable->FlipX(true);
	leftBowDrawable->SetVisibility(false);
	leftBow->AddComponent(leftBowDrawable);

	GcPhysical *leftBowPhys = new GcPhysical();
	leftBowPhys->InitializeShapes(48.f / pixelsPerMeter,
		600.f / pixelsPerMeter,
		13.f / pixelsPerMeter,
		24.f / pixelsPerMeter,
		false, _world, false);

	leftBow->AddComponent(leftBowPhys);

	player->AddChild("leftBow", leftBow);

	WeldJoint(playerPhys, leftBowPhys, -8.5f / pixelsPerMeter, 3.f / pixelsPerMeter);
	_characters.push_back(player);
	return player;
}

void Level::SetKillHandler(IKillHandler* killHandler)
{
	_killHandler = killHandler;
}