#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include<string>
#include<map>


struct State
{
	std::string StateName;
	float TotalTime;
	float TimeLeft;
	std::map<std::string, State*> States;
	bool Locked;
};

class StateMachine
{
public:
	StateMachine();
	~StateMachine();
	void Update(float secondsElapsed);
	bool GoToState(std::string stateName);
	std::string GetCurrentStateName() const;
private:
	State* _rootState;
	State* _currentState;
};

#endif