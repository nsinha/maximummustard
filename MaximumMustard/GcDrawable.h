#ifndef GAME_COMPONENT_H
	#include "GameComponent.h"
#endif

class GcDrawable :
	public GameComponent
{
public:
	GcDrawable();
	~GcDrawable();
	virtual void Update(float secondsElapsed);
	unsigned int GetTextureId() const;
	void SetTextureId(unsigned int textureId);
	float GetX() const;
	float GetY() const;
	float GetAngle() const;
	void FlipX(bool flipped);
	bool IsFlipped() const;
	void SetVisibility(bool visible);
	bool IsVisible() const;
private:
	float _x, _y, _angle;
	unsigned int _textureId;
	bool _flipped;
	bool _visible;
};

