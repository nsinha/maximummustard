#ifndef GAME_SCREEN_H
#define GAME_SCREEN_H

#include<vector>
#include<Box2D/Box2D.h>
#include<SFML/Graphics.hpp>


#ifndef BASE_SCREEN_H
	#include "BaseScreen.h"
#endif

#ifndef GAME_MANAGER_H
	#include"GameManager.h"
#endif

class Level;
class GameObject;

class GameScreen :
	public BaseScreen
{
public:
	GameScreen(sf::RenderWindow* window, std::vector<int> players);
	virtual void Initialize();
	virtual bool Update();
	virtual void Draw();
	virtual ~GameScreen();

private:
	void InitializeLevel();
	Level* _level;
	b2Body* playerBody;
	GameManager _gameManager;
	sf::Font _textFont;
	const float pixelsPerMeter = 32.f;
};

#endif