#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include<map>

template <class T>
class TextureManager
{
public:
	TextureManager();
	~TextureManager();
	void Add(unsigned int id, T* texture);
	T* Get(unsigned int id) const;
	void Clear();
private:
	std::map<unsigned int, T*> _textures;
};

template <class T>
TextureManager<T>::TextureManager()
{
}

template <class T>
TextureManager<T>::~TextureManager()
{
	Clear();
}

template <class T>
void TextureManager<T>::Add(unsigned int id, T* texture)
{
	_textures[id] = texture;
}

template <class T>
T* TextureManager<T>::Get(unsigned int id) const
{
	auto found = _textures.find(id);
	if (found == _textures.end())
	{
		return nullptr;
	}
	return found->second;
}

template <class T>
void TextureManager<T>::Clear()
{
	for (auto iter = _textures.begin(); iter != _textures.end(); ++iter)
	{
		delete iter->second;
	}

	_textures.clear();
}

#endif