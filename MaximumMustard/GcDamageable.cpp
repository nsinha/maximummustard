#include "GcDamageable.h"


GcDamageable::GcDamageable(int maxHealth) : _maxHealth(maxHealth),
											_currentHealth(maxHealth),
											GameComponent(GameComponentGroup::Damageable)
{
	AddType(GameComponentType::Damageable);
}


GcDamageable::~GcDamageable()
{
}


bool GcDamageable::TakeDamage(int damage)
{
	_currentHealth -= damage;
	if (_currentHealth <= 0)
	{
		_currentHealth = 0;
		return true;
	}
	return false;
}

int GcDamageable::GetHealth() const
{
	return _currentHealth;
}

int GcDamageable::GetMaxHealth() const
{
	return _maxHealth;
}

void GcDamageable::Update(float dt)
{
	
}