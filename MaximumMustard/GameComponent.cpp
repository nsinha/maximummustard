#include "GameComponent.h"



GameComponent::GameComponent(GameComponentGroup gcGroup) : _gcGroup(gcGroup), _gcType(GameComponentType::Missing)
{
}


GameComponent::~GameComponent()
{
}

GameComponentGroup GameComponent::GetComponentGroup() const
{
	return _gcGroup;
}

void GameComponent::SetGameObject(GameObject* go)
{
	_go = go;
}

GameObject* GameComponent::GetGameObject() const
{
	return _go;
}

bool GameComponent::IsOfType(GameComponentType gcType) const
{
	return (_gcType & gcType) > 0;
}

void GameComponent::AddType(GameComponentType gcType)
{
	_gcType = static_cast<GameComponentType>(_gcType | gcType);
}