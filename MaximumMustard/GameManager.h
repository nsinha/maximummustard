#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#ifndef I_KILL_HANDLER_H
	#include "IKillHandler.h"
#endif

#include <SFML/Graphics/View.hpp>
#include <vector>

#ifndef I_GAME_MANAGER_H
	#include "IGameManager.h"
#endif

class Camera;
class GameObject;
class Hud;

struct PlayerInfo
{
	int PlayerId;
	int Kills;
	int Deaths;
	sf::View PlayerView;
	GameObject *Go;
	Camera* Camera;
};

class GameManager : public IKillHandler, public IGameManager
{
public:
	GameManager();
	std::vector<PlayerInfo>& GetPlayers();
	void AddPlayer(PlayerInfo pi);
	
	//IKillHandler
	virtual void HandleKill(int killer, int killee) override;

	//IGameManager
	virtual void ChangePlayerTarget(int playerId, GameObject* go) override;
	
	bool CheckForVictory() const;

	~GameManager();
private:
	std::vector<PlayerInfo> _players;
};

#endif