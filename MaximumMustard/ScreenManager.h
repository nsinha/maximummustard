#ifndef SCREEN_MANAGER_H
#define SCREEN_MANAGER_H

class BaseScreen;

class ScreenManager
{
public:
	static ScreenManager& GetInstance()
	{
		static ScreenManager instance;
		return instance;
	}
	~ScreenManager();
	void SwitchToScreen(BaseScreen* screen);
	BaseScreen* GetCurrentScreen();
private:
	ScreenManager();
	BaseScreen* _currentScreen;
};

#endif